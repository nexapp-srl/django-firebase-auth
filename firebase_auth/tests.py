from django.test import RequestFactory, TestCase
from .authentication import FirebaseAuthentication
from django.contrib.auth import get_user_model
from unittest import mock
User = get_user_model()


class FirebaseAuthenticationTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.auth = FirebaseAuthentication()

    @mock.patch('firebase_admin.auth.verify_id_token', return_value={'uid': 'useruid'})
    def test_auth_not_existing_user(self, _):
        request = self.factory.get(
            '/', AUTHORIZATION='FIREBASE firebaseusertoken')
        user, auth = self.auth.authenticate(request)
        self.assertIsNotNone(user)
        self.assertIsNone(auth)
        self.assertTrue(User.objects.filter(username=user.username).exists())

    @mock.patch('firebase_admin.auth.verify_id_token', return_value={'uid': 'useruid'})
    def test_auth_existing_user(self, _):
        user_created = User.objects.create(username='useruid')
        request = self.factory.get(
            '/', AUTHORIZATION='FIREBASE firebaseusertoken')
        user_authenticated, auth = self.auth.authenticate(request)
        self.assertIsNotNone(user_authenticated)
        self.assertIsNone(auth)
        self.assertEqual(user_created, user_authenticated)

    def test_auth_no_http_authorization(self):
        request = self.factory.get('/')
        res = self.auth.authenticate(request)
        self.assertIsNone(res)


    def test_auth_bearer(self):
        request = self.factory.get(
            '/', AUTHORIZATION='BEARER bearertoken')
        user = self.auth.authenticate(request)
        self.assertIsNone(user)
