import firebase_admin
from firebase_admin import credentials, auth
from rest_framework import authentication
from django.contrib.auth import get_user_model
from django.conf import settings
User = get_user_model()

try:
    cred = credentials.Certificate(settings.FIREBASE_SERVICE_ACCOUNT_KEY)
    default_app = firebase_admin.initialize_app(cred)
except:
    pass


class FirebaseAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        if not hasattr(settings, 'FIREBASE_SERVICE_ACCOUNT_KEY'):
            return None

        try:
            authorization = request.META.get('AUTHORIZATION')

            if authorization.startswith('FIREBASE '):
                id_token = authorization.replace('FIREBASE ', '', 1)
            else:
                return None
        except:
            return None

        try:
            decoded_token = auth.verify_id_token(id_token)
            uid = decoded_token['uid']
            user, _ = User.objects.get_or_create(username=uid)
            return (user, None)
        except:
            raise exceptions.AuthenticationFailed('Invalid Firebase ID Token')