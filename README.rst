=====
django-firebase-auth
=====

django-firebase-auth is a authentication provider for Google's `Firebase Authentication Service <https://firebase.google.com/products/auth/>`_ which blends right into django-rest-framework.
Simply setup your Firebase keyfile location and mark views which need authentication with the FirebaseAuthMixin authentication class.


Detailed documentation is in the "docs" directory.

Installation
-----------

Install via pip::

    pip install git+https://gitlab.com/nexapp-srl/django-firebase-auth
or add this line to requirements.txt if using Docker::

    git+https://gitlab.com/nexapp-srl/django-firebase-auth

(This will also install dependencies `firebase-admin <https://github.com/firebase/firebase-admin-python/>`_.)

Quick start
-----------

1. Add "firebase_auth" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'firebase_auth',
    ]

2. Specify a location for your Firebase keyfile with the settings::

    # Firebase service account key
    FIREBASE_SERVICE_ACCOUNT_KEY_PATH = 'path-to-key-file'
  for example::

    FIREBASE_SERVICE_ACCOUNT_KEY_PATH = ROOT_DIR(env.str('FIREBASE_SERVICE_ACCOUNT_KEY_PATH'))

3. Set FirebaseAuthentication as the global default authentication class in settings, like::
    
    REST_FRAMEWORK = {
        DEFAULT_AUTHENTICATION_CLASSES': ('firebase_auth.authentication.FirebaseAuthentication', ),
    }

4. The authenticator automatically creates the user if not exists with the Firebase user ID as username.
   Inside your views, you can access the user reference like you're used to with  request.user
   
