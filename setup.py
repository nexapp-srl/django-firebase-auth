import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-firebase-auth',
    version='0.0.1',
    packages=find_packages(),
    install_requires=[
          'firebase-admin',
          'django',
          'djangorestframework',
    ],
    include_package_data=True,
    description='A django authentication provider for Google\'s Firebase Authentication Service',
    long_description=README,
    author='Andrea Cattaneo',
    author_email='acattaneo@nexapp.it',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
    ],
)